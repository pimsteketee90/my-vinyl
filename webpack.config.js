const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
let FaviconsWebpackPlugin = require('favicons-webpack-plugin')
let WebpackPwaManifest = require('webpack-pwa-manifest')

module.exports = {
    entry: { main: './src/js/index.js' },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.pug$/,
                use: [
                    'html-loader?attrs=false',
                    'pug-html-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract(
                    {
                        fallback: 'style-loader',
                        use: [{
                            loader: 'css-loader', options: {
                                sourceMap: true
                            }
                        }, {
                            loader: 'sass-loader', options: {
                                sourceMap: true
                            }
                        }]
                    })
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: [
                    "file-loader",
                    {
                        loader: "image-webpack-loader",
                        options: {

                        }
                    }
                ]
              }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new ExtractTextPlugin({
            filename: 'main.css'
        }),
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/html/index.pug',
            filename: 'index.html'
        }),

        new WebpackPwaManifest({
            name: 'My Vinyl',
            short_name: 'My Vinyl',
            description: 'A resume of my vinyl collection',
            background_color: 'red',
            display: "fullscreen",
            crossorigin: null, //can be null, use-credentials or anonymous
            icons: [
                {
                    src: path.resolve('src/favicon/favicon-source.png'),
                    sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
                },
                {
                    src: path.resolve('src/favicon/favicon-source.png'),
                    size: '1024x1024' // you can also use the specifications pattern
                }
            ]
        }),

        new FaviconsWebpackPlugin('./src/favicon/favicon-source.png')
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};