export class Images {
    constructor() {
        this.images = document.querySelectorAll(".vinyl-artwork")
        this.activateArtwork()
    }

    activateArtwork() {
        for (let i = 0, l = this.images.length; i < l; i++) {
            this.images[i].addEventListener("click", (e) => {
                document.body.classList.toggle("js-body-artwork")
                this.images[i].closest(".list-item").classList.toggle("js-artwork-active")
                
                this.closeArtwork()
            })            
        }
    }

    closeArtwork() {
        this.closeIcon = document.querySelector(".close-artwork")
        const activeArtworkListItem = document.querySelector(".js-artwork-active")
        
        this.closeIcon.addEventListener("click", (e) => {
            this.handleCloseEvent(activeArtworkListItem)
        })

        this.closeIcon.addEventListener("touchstart", (e) => {
            this.handleCloseEvent(activeArtworkListItem)
        })
    }

    handleCloseEvent(activeListItem) {
        activeListItem.classList.remove("js-artwork-active")
        document.body.classList.remove("js-body-artwork")
    }
}