export class Search {
    constructor() {
        this.searchBarInput = document.querySelector(".searchbar-input")
        this.resetInput = document.querySelector(".js-reset-input")
        this.vinylList = document.querySelector(".vinyl-list")
        this.vinylListItems = [...document.querySelectorAll(".vinyl-list li")]

        this.searchResultsList = document.querySelector(".search-results-list")
        this.searchResultsCounter = document.querySelector(".js-search-results-counter")
        this.searchResultsText = document.querySelector(".js-search-results-text")
        this.searchResultsCounter.innerHTML = `${this.vinylListItems.length}`

        this.showSearchResults()
        this.resetSearchBarInput()
    }

    showSearchResults() {
        this.searchBarInput.addEventListener("keyup", () => {
        const searchBarInput = document.querySelector(".searchbar-input")

            // check for similarities between the input value and the text inside list-items
            function getSearchResults(searchResult) {
                return searchResult.textContent.toUpperCase().includes(`${searchBarInput.value.toUpperCase()}`)
            }

            this.searchResultsList.innerHTML = ""

            let results = this.vinylListItems.filter(getSearchResults)

            for (let i = 0, result; result = results[i]; i++) {
                this.searchResultsList.appendChild(result)
            }

            this.searchResultsCounter.innerHTML = `${results.length}`

            if (this.searchBarInput.value === "") {
                this.initialState()

                this.searchResultsText.innerHTML = "items"
            }

            else {
                this.searchResultsText.innerHTML = "results"

                if (results.length === 0) {
                    this.searchResultsCounter.innerHTML = "no"
                }

                if (results.length === 1) {
                    this.searchResultsText.innerHTML = "result"
                }
            }
        })
    }

    resetSearchBarInput() {
        this.resetInput.addEventListener("click", (e) => {
            e.preventDefault()
            this.searchBarInput.value = ""
            this.searchBarInput.focus()

            this.initialState()
        })
    }

    initialState() {
        this.searchResultsList.innerHTML = ""
        this.searchResultsCounter.innerHTML = `${this.vinylListItems.length}`
        this.searchResultsText.innerHTML = "items"

        for (let i = 0, item; item = this.vinylListItems[i]; i += 1) {
            this.vinylList.appendChild(item)
        }
    }
}