export class NavBottom {
    constructor() {
        this.vinylListComponent = document.querySelector(".vinyl-list-component")
        this.vinylList = document.querySelector(".vinyl-list")
        this.vinylListItems = [...document.querySelectorAll(".vinyl-list li")]
        this.inactiveList = document.querySelector(".inactive-list")
        this.inactiveListItems = [...document.querySelectorAll(".vinyl-list .inactive")]
        this.searchComponent = document.querySelector(".search-component")
        this.wishList = document.querySelector(".wishlist-component")
        this.inactiveListComponent = document.querySelector(".inactive-list-component")
        this.navBottomMyVinyl = document.querySelector(".js-my-vinyl-link")
        this.navBottomSearch = document.querySelector(".js-search-link")
        this.navBottomWishList = document.querySelector(".js-wishlist-link")
        this.navBottomInactiveList = document.querySelector(".js-inactive-link")
        this.searchBarInput = document.querySelector(".searchbar-input")
        this.searchResultsCounter = document.querySelector(".js-search-results-counter")
        this.searchResultsText = document.querySelector(".js-search-results-text")

        this.showMyVinyl()
        this.showSearch()
        this.showWishList()
        this.showInactiveList()
    }

    showMyVinyl() {
        if (this.navBottomMyVinyl) {
            this.navBottomMyVinyl.addEventListener("click", (e) => {
                e.preventDefault()

                this.vinylListComponent.classList.add("visible")
                this.vinylListComponent.classList.remove("hidden")
                this.searchComponent.classList.remove("visible")
                this.searchComponent.classList.add("hidden")
                this.wishList.classList.remove("visible")
                this.inactiveListComponent.classList.add("hidden")
                this.inactiveListComponent.classList.remove("visible")
                this.navBottomMyVinyl.classList.add("active")
                this.navBottomSearch.classList.remove("active")
                this.navBottomInactiveList.classList.remove("active")
                this.navBottomWishList.classList.remove("active")            

                for (let i = 0, item; item = this.vinylListItems[i]; i += 1) {
                    this.vinylList.appendChild(item)
                }
            })
        }
    }

    showSearch() {
        if (this.navBottomSearch) {
            this.navBottomSearch.addEventListener("click", (e) => {
                e.preventDefault()

                this.vinylListComponent.classList.remove("visible")
                this.vinylListComponent.classList.add("hidden")
                this.inactiveListComponent.classList.add("hidden")
                this.inactiveListComponent.classList.remove("visible")
                this.wishList.classList.remove("visible")
                this.searchComponent.classList.add("visible")
                this.searchComponent.classList.remove("hidden")
                this.navBottomSearch.classList.add("active")
                this.navBottomMyVinyl.classList.remove("active")
                this.navBottomWishList.classList.remove("active")
                this.navBottomInactiveList.classList.remove("active")
                this.searchResultsCounter.innerHTML = `${this.vinylListItems.length}`
                this.searchBarInput.value = ""
                this.searchResultsText.innerHTML = "items"
            })
        }
    }

    showWishList() {
        if (this.navBottomWishList) {
            this.navBottomWishList.addEventListener("click", (e) => {
                e.preventDefault()

                this.vinylListComponent.classList.remove("visible")
                this.searchComponent.classList.add("hidden")
                this.inactiveListComponent.classList.add("hidden")
                this.inactiveListComponent.classList.remove("visible")
                this.searchComponent.classList.remove("visible")
                this.wishList.classList.remove("hidden")
                this.wishList.classList.add("visible")
                this.navBottomWishList.classList.add("active")
                this.navBottomInactiveList.classList.remove("active")
                this.navBottomSearch.classList.remove("active")
                this.navBottomMyVinyl.classList.remove("active")
            })
        }
    }

    showInactiveList() {
        if (this.navBottomInactiveList) {
            this.navBottomInactiveList.addEventListener("click", (e) => {
                e.preventDefault()

                this.vinylListComponent.classList.remove("visible")
                this.searchComponent.classList.add("hidden")
                this.searchComponent.classList.remove("visible")
                this.inactiveListComponent.classList.remove("hidden")
                this.inactiveListComponent.classList.add("visible")
                this.wishList.classList.remove("visible")
                this.navBottomInactiveList.classList.add("active")
                this.navBottomWishList.classList.remove("active")
                this.navBottomSearch.classList.remove("active")
                this.navBottomMyVinyl.classList.remove("active")

                for (let i = 0, item; item = this.inactiveListItems[i]; i += 1) {
                    this.inactiveList.appendChild(item)
                }
            })
        }
    }
}