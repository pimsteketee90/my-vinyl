export class inactiveList {
	constructor() {
        this.inactiveListItems = [...document.querySelectorAll(".vinyl-list .inactive")]
		this.inactiveListItemsCounter = document.querySelector(".js-inactive-items-counter")

        this.countListItems()
    }

	countListItems() {
		if (this.inactiveListItemsCounter) {
			this.inactiveListItemsCounter.innerHTML = `${this.inactiveListItems.length}`
		}
	}
}