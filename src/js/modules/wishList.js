export class WishList {
	constructor() {
        this.wishList = document.querySelector(".wishlist")
		this.wishListItems = [...document.querySelectorAll(".wishlist li")]
		this.wishListCounter = document.querySelector(".js-wishlist-counter")

        this.sortWishList()
    }
    
    sortWishList() {
		// comparator for text content
		const comparator = (a, b) => {
			const { textContent: aTextContent } = a
			const { textContent: bTextContent } = b
			return (aTextContent < bTextContent ? -1 : (aTextContent > bTextContent ? 1 : 0))
		}

		// sort the items in the list
		this.wishListItems.sort(comparator)

		// empty the list
		this.wishList.innerHTML = ''

		// loop through the items and append them to the list
		for (let i = 0, item; item = this.wishListItems[i]; i += 1) {
			this.wishList.appendChild(item)
		}

		// count the amount of list-items
		this.countListItems()
	}

	countListItems() {
		if (this.wishListCounter) {
			this.wishListCounter.innerHTML = `${this.wishListItems.length}`
		}
	}
}