export class VinylList {
	constructor() {
		this.vinylList = document.querySelector(".vinyl-list")
		this.vinylListcounter = document.querySelector(".js-vinyl-list-counter")
		this.vinyllistItems = [...document.querySelectorAll(".vinyl-list li")]

        this.sortVinylList()
	}

	sortVinylList() {
		// comparator for text content
		const comparator = (a, b) => {
			const { textContent: aTextContent } = a
			const { textContent: bTextContent } = b
			return (aTextContent < bTextContent ? -1 : (aTextContent > bTextContent ? 1 : 0))
		}

		// sort the items in the list
        this.vinyllistItems.sort(comparator)

		// empty the list
		this.vinylList.innerHTML = ''

		// loop through the items and append them to the list
		for (let i = 0, item; item = this.vinyllistItems[i]; i += 1) {
            this.vinylList.appendChild(item)
		}

		// count the amount of list-items
		this.countListItems()
	}

	countListItems() {
		this.vinylListcounter.innerHTML = `${this.vinyllistItems.length}`
	}
}