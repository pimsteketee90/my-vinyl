import _ from 'lodash';
import '../css/main.scss';

// import modules
import { NavBottom } from './modules/navBottom'
import { VinylList } from "./modules/vinylList"
import { WishList } from "./modules/wishList"
import { Search } from "./modules/search"
import { inactiveList } from "./modules/inactiveList"
import { Images } from "./modules/images"

new VinylList()
new NavBottom()
new WishList()
new Search()
new inactiveList()
new Images()